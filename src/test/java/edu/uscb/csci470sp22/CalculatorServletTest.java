package edu.uscb.csci470sp22;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorServletTest {

	@Test
	public void testAtLeastOneStringIsNullPositive() {
		assertTrue(CalculatorServlet.atLeastOneStringIsNull(null, "", ""));
	}

	@Test
	public void testAtLeastOneStringIsNullNegative() {
		assertFalse(CalculatorServlet.atLeastOneStringIsNull("1", "add", "3"));
	}
	
	@Test
	public void testAtLeastOneTrimmedStringIsEmptyPositive() {
		assertTrue(CalculatorServlet.atLeastOneTrimmedStringIsEmpty("foo", "1", "   "));
	}

	@Test
	public void testAtLeastOneTrimmedStringIsEmptyNegative() {
		assertFalse(CalculatorServlet.atLeastOneTrimmedStringIsEmpty("2", "subtract", "1"));
	}
	
	@Test
	public void testStringDoesNotMatchAllowedValuesPositive() {
		assertTrue(CalculatorServlet.stringDoesNotMatchAllowedValues("5; DROP TABLE users"));
	}

	@Test
	public void testStringDoesNotMatchAllowedValuesNegative() {
		assertFalse(CalculatorServlet.stringDoesNotMatchAllowedValues("multiply"));
	}
	
	@Test
	public void testStringCannotBeConvertedToNumberPositive() {
		assertTrue(CalculatorServlet.stringCannotBeConvertedToNumber("three"));
	}

	@Test
	public void testStringCannotBeConvertedToNumberNegative() {
		assertFalse(CalculatorServlet.stringCannotBeConvertedToNumber("3.14159"));
	}
	
	@Test
	public void testLongDivisionAttemptedWithFloatingPointValuesPositive() {
		assertTrue(CalculatorServlet.longDivisionAttemptedWithFloatingPointValues("3", "5.1", "longDivision"));
	}
	
	@Test
	public void testLongDivisionAttemptedWithFloatingPointValuesNegative() {
		assertFalse(CalculatorServlet.longDivisionAttemptedWithFloatingPointValues("3", "5", "longDivision"));
	}
	
} // end class CalculatorServletTest