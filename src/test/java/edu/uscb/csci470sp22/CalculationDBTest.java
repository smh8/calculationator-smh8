package edu.uscb.csci470sp22;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.sql.SQLException;
import org.junit.Test;

public class CalculationDBTest {

	@Test
	public void testInsert() throws SQLException {
		
		// Create test user and calculation objects
		User user = new User("mockuser1@testdomain.com","gibberish");
		
		Calculation calculation = 
	    		new Calculation.CalculationBuilder(
	    				2, 3, "add")
	    		.result()
	    		.build();
		
		int expectedResult = 1;
		int testResult = 0;

		try {
			testResult = 
				CalculationDB.insert(user.getEmail(), calculation); // should return 1 if insert works
		} catch (URISyntaxException e) { 
			e.printStackTrace();
		}
		assertEquals( expectedResult, testResult );
	} // end method testInsert (for calculation)

	@Test
	public void testGetCalculationHistoryForUser() {
		// Create test user and calculation objects
		// NOTE: you will need to have entered the data manually beforehand
		//       using pgAdmin (also the user will need to have been insert
		//       into the DB **BEFORE** the calculation or it won't work)
		// ...OR use an Java "mocking" package like Mockito, jOOQ, etc.
		
		User user = new User("mockuser2@testdomain.com","gibberish");
		
		// Note that the calculation history will be rendered in the
		// REVERSE of the order in which it is stored in the database
		String expectedResultHtml = "<div class='history'>"
				                    + "<p>Your Calculation History</p>" 
				                    + "2.0 - 1.0 = 1.0<br>"
				                    + "1.0 + 1.0 = 2.0<br></div>";
		String testResultHtml = "";
		
		try {
			testResultHtml = CalculationDB.getCalculationHistoryForUser( user.getEmail() );
		} catch (URISyntaxException | SQLException e) {
			e.printStackTrace();
		}
		
		assertEquals(expectedResultHtml, testResultHtml);
		
	} // end method testGetCalculationHistoryForUser
	
} // end class CalculationDBTest