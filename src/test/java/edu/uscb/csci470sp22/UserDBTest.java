package edu.uscb.csci470sp22;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.sql.SQLException;

import org.junit.Test;

public class UserDBTest {

	@Test
	public void testInsert() throws SQLException, URISyntaxException {
		User user = new User("mockuser1@testdomain.com","gibberish");
		int expectedResult = 1;
		int testResult = 0;
		
		testResult = UserDB.insert(user); // should return 1 if insert works
		assertEquals( expectedResult, testResult );
	} // end method testInsert (for user)
	
	@Test
	public void testCheckLogin() throws 
			URISyntaxException, SQLException {
		// NOTE: this user should have been manually inserted beforehand,
		//       via pgAdmin 
		String email = "mockuser2@testdomain.com";
		String password = "gibberish";

		// will return null if the user doesn't exist in DB
		User testUser = UserDB.checkLogin(email, password);

		String expectedResult = email + " " + password;
		String testResult = testUser.getEmail() + " " + testUser.getPassword();
		
		assertEquals( expectedResult, testResult );
	} // end method testCheckLogin 
	
	@Test
	public void testUserExists() throws URISyntaxException, SQLException {
		
		String email = "mockuser2@testdomain.com";
		
		boolean expectedResult = true;
		boolean testResult = UserDB.userExists(email);
		
		assertEquals( expectedResult, testResult );
		
	} // end method testUserExists
	
} // end class UserDBTest