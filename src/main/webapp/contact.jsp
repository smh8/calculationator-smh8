<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="includes/header.jsp" />
	
<!-- main content starts here -->
<main>
  <div class="contact-login-form-container">
    <p>Need help? Got an idea for a feature? Contact us!</p>
    <form action="process-contact" method="post" id="contact-form">
    	<input type="hidden" name="action" value="send-email">
    	<label for="name">Your Name:</label><br>
		<input type="text" name="name" size="30" />
		<label for="email" class="add-top-margin">Email Address:</label><br>
		<input type="email" name="email" size="30" />
		<label for="message" class="add-top-margin">Your Message:</label><br>
		<textarea rows="5" cols="30" name="message" style="resize: none;"></textarea>
		<br>	
		<input type="submit" value="Send message" class="large-button">
	</form>
	${message}
  </div> <!-- contact-login-form-container -->
</main>

<c:import url="includes/footer.jsp" />

</body>
</html>