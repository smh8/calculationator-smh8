<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="includes/header.jsp" />
	
<!-- main content starts here -->
<main>
  <div class="contact-login-form-container">
    <p>Register for a FREE account to save &amp; view your calculation history!</p>
    <form action="process-registration" method="post" id="registration-form">
    	<input type="hidden" name="action" value="register-new-user">
		<label for="email">Email Address:</label><br>
		<input type="email" name="email" size="30" />
		<br>	
		<input type="submit" value="Sign up" class="large-button">
	</form>
	${message}
	<p><small>Already have an account? <a href="login.jsp">Click here to login!</a></small></p>
  </div> <!-- contact-login-form-container -->
</main>

<c:import url="includes/footer.jsp" />

</body>
</html>