<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="includes/header.jsp" />
	
<!-- main content start here -->
<main>
  <div class="about-container">
    <h4><b>What is &ldquo;The Calculationator&rdquo;?</b></h4>
    <p><b>The Calculationator</b> is a revolutionary new way to do simple
    arithmetic calculations, right in your web browser. You may never need
    a handheld calculator again!</p>
    <h4><b>How does it work?</b></h4>
    <p>It's super easy! Just enter two numbers and select the type 
    of arithmetic operation you want to perform on them, and then 
    click the <b>Calculationate!</b> button to display the result.
    <a href="index.jsp">Try it!</a>
    </p>
    <p>
    	<b>The Calculationator</b> presently supports five types of arithmetic operations:
	    <small><ul>
	    <li><b>Addition</b> of two numbers</li>
	    <li><b>Subtraction</b> of one number from another</li>
	    <li><b>Multiplication</b> of two numbers</li>
	    <li><b>Division</b> of one number by another (supports values with a decimal point)</li>
	    <li><b>Long division</b> of one whole number into another (with a whole-number remainder)</li>
	    </ul></small>
    </p>
    <h4><b>Can I save or view my calculation results?</b></h4>
    <p>You sure can! Simply <a href="register.jsp">register</a> for a FREE account,
    and then while you are logged in, <b>The Calculationator</b> will
    store each new calculation result while displaying your
    previous calculation history at the bottom of the page!</p>
    <h4><b>Can I request a feature or get help?</b></h4>
    <p>Sure you can &mdash; just click on the
    <a href="contact.jsp">Contact Us</a> link and 
    fill out our simple form!</p>
  </div> <!-- about-container -->
</main>

<c:import url="includes/footer.jsp" />

</body>
</html>