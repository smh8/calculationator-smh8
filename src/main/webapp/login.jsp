<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="includes/header.jsp" />
	
<!-- main content starts here -->
<main>
  <div class="contact-login-form-container">
    <p>Got an account? Log in to save &amp; view your calculation history!</p>
    <form action="process-login" method="post" id="login-form">
    	<input type="hidden" name="action" value="authenticate-user">
		<label for="email">Email Address:</label><br>
		<input type="email" name="email" size="30" />
		<br>
		<label for="password" class="add-top-margin">Password:</label><br>
		<input type="password" name="password" size="30" />
		<br>	
		<input type="submit" value="Log in" class="large-button">
	</form>
	${message}
	<p><small>Don't have an account? <a href="register.jsp">Click here to register!</a></small></p>
  </div> <!-- contact-login-form-container -->
</main>

<c:import url="includes/footer.jsp" />

</body>
</html>