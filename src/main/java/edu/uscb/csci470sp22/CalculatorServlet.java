package edu.uscb.csci470sp22;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CalculatorServlet
 */
public class CalculatorServlet extends HttpServlet {
	
	private static final long serialVersionUID = -3015559252805509180L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false); // don't create new session if not exist already		
		
		// set the default JSP to load for this servlet
		String url = "/index.jsp";
		
		// get current action
		String action = request.getParameter("action");
	    if (action == null) {
	    	action = "startover"; // default action
	    } // end if
	    
	    // perform action and set URL to appropriate page
	    if (action.equals("startover")) {
	    	url = "/index.jsp";
		} else if (action.equals("calculate")) { 
	    	// get other parameters from the request
	    	String firstNumberStr = request.getParameter("firstNumber");
	    	String secondNumberStr = request.getParameter("secondNumber");
	    	String operation = request.getParameter("operation");
	    	
	    	// perform validation to make sure proper values are entered
	    	// TODO: Move this to a method and test using assertTrue/assertFalse
	    	String message;
	    	try {
	    		if ( atLeastOneStringIsNull(
	    				firstNumberStr, secondNumberStr, operation)
	    			 || atLeastOneTrimmedStringIsEmpty(
	    					 firstNumberStr, secondNumberStr, operation) ) {
	    			message = "Please enter two numbers and choose an operation.";
	    			url = "/index.jsp";
	    		} else if ( stringDoesNotMatchAllowedValues(operation) ) {
	    			message = "Please use the drop-down menu to choose one of " 
	    					+ "the available operations: add, subtract, " 
	    					+ "multiply, divide, or long division.";
	    			url = "/index.jsp";
	    		} else if ( stringCannotBeConvertedToNumber(firstNumberStr) 
	    				    || stringCannotBeConvertedToNumber(secondNumberStr) ) {
	    			message = "Only numeric values are allowed. Please try again!";
	    			url = "/index.jsp";
	    		} else if ( longDivisionAttemptedWithFloatingPointValues(
	    				        firstNumberStr, secondNumberStr, operation) ) {
	    			message = "Only whole numbers (no decimal points) are "
	    					  + "permitted for long division. Please try again!";
	    			url = "/index.jsp";
	    	    } else {
	    	    	
	    	    	double firstNumber = Double.parseDouble( firstNumberStr );
	    	    	double secondNumber = Double.parseDouble( secondNumberStr );
	    	    	
	    	    	// Use Builder design pattern to instantiate a Calculation
	    	    	// object and compute its result 
	    	    	Calculation calculation = 
	    	    		new Calculation.CalculationBuilder(
	    	    				firstNumber, secondNumber, operation)
	    	    		.result()
	    	    		.build();
	    	    	
	    	    	url = "/index.jsp";
	    	    	
	    	    	message = "<small>Result:</small><br>" 
	    	    	   + calculation.getNum1() + " " 
	    	    	   + calculation.getOperatorSymbolHtml() + " "
	    	    	   + calculation.getNum2() + " = " 
	    	    	   + calculation.getResult();
	    	    	
	    	    	// Insert if logged in (need to add user as argument)
	    	    	User user = (User)session.getAttribute("user");
	    	    	if ( user != null ) {
	    	    		CalculationDB.insert(user.getEmail(), calculation);
	    	    	} // end inner if
	    	    	
	    	    } // end multi-way if/else (for try block)
	    		
	    	} catch ( Exception e ) { // if any exceptions are thrown
	    		message = "Something went wrong. Please try again!";
	    		url = "/index.jsp";
	    	} // end try statement for validating/handling form inputs 
	    	
			// Get the updated calculation history
	    	User user = (User)session.getAttribute("user");
	    	String history = "";
	    	if ( user != null ) {
	    		// get user's calculation history
				try {
					history = CalculationDB.getCalculationHistoryForUser(user.getEmail());
				} catch (URISyntaxException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	session.setAttribute("history", history);
	    	} // end if
	    	
	    	// Set attributes to pass data to the JSP file
	    	request.setAttribute("message", message);
	    	session.setAttribute("history", history);

	    } // end if/else (for determining controller action)
	    
	    // open the JSP file
	    getServletContext()
	        .getRequestDispatcher(url)
	        .forward(request, response);
	    
	} // end method doGet

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	/**
	 * 
	 * @param listOfStrings 
	 * @return true if at least one String is null, false otherwise
	 */
	public static boolean atLeastOneStringIsNull( String... listOfStrings ) {
		for ( String currentString : listOfStrings ) {
			if ( currentString == null ) {
				return true;
			} // end if
		} // end for
		return false;
	} // end method atLeastOneInputIsNull
	
	/**
	 * 
	 * @param listOfStrings 
	 * @return true if any string is empty after stripping whitespace, false otherwise
	 */
	public static boolean atLeastOneTrimmedStringIsEmpty( String... listOfStrings ) {
		for ( String currentString : listOfStrings ) {
			if ( currentString.strip().isEmpty() ) {
				return true;
			} // end if
		} // end for
		return false;
	} // end method atLeastOneTrimmedStringIsEmpty
	
	/**
	 * 
	 * @param testString
	 * @return true if String is not one of the permissible operations
	 */
	public static boolean stringDoesNotMatchAllowedValues( String testString ) {
		return !testString.matches("add|subtract|multiply|divide|longDivision");
	} // end method stringDoesNotMatchAllowedValues
	
	
	/**
	 * 
	 * @param testString
	 * @return true if string is anything but a numeric value, false otherwise
	 */
	public static boolean stringCannotBeConvertedToNumber( String testString ) {
		return !(testString.strip().matches(
				"^(?!-0?(\\.0+)?$)-?(0|[1-9]\\d*)?(\\.\\d+)?(?<=\\d)$"));
	} // end method stringCannotBeConvertedToNumber
	
	/**
	 * 
	 * @param divisorStr
	 * @param dividendStr
	 * @param operation
	 * @return ture if longDivision is selected and either operand contains a decimal point
	 */
	public static boolean longDivisionAttemptedWithFloatingPointValues( 
			String divisorStr, String dividendStr, String operation ) {
		if ( operation.equalsIgnoreCase("longDivision") ) {
			if ( divisorStr.contains(".") || dividendStr.contains(".") ) {
				return true;
			} // end inner if
		} // end outer if
		return false;
	} // end method longDivisionAttemptedWithFloatingPointValues
	
} // end class CalculatorServlet