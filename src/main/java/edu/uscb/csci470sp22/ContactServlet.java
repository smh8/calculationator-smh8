package edu.uscb.csci470sp22;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/* Note that this class uses the more modern approach to mapping
 * servlets to URL patterns; we used the older/legacy approach for
 * CalculatorServlet.java (see src/main/webapp/WEB-INF/web.xml), but
 * I wanted to show you this "annotation-style" approach as well, just
 * to ensure you have knowledge of both. :-)
 */
@WebServlet("/process-contact")
public class ContactServlet extends HttpServlet {
       
	private static final long serialVersionUID = 1183167924265772927L;

	/*
	 * Including doGet here ensures that if someone tries browsing to
	 * the /process-contact link (without submitting a form), then
	 * they will be automatically redirected to the contact.jsp page
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	} // end method doGet
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// get current action
        String action = request.getParameter("action");

		String url = "/contact.jsp";
		String message = "";
		boolean redirectStatus = false; 
		
        if (action == null) {
        	url = "/contact.jsp"; // redirect if user visits /process-contact
        	redirectStatus = true;
        } else if (action.equals("send-email")) { 
		
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			String smtpEmail = "calculationator.smh8@yahoo.com";
			String subject = "New message from "+ name 
					         + " via The Calculationator contact form";
			String body = name + " (" + email + ") has sent you the following "
					      + "message via the Contact Us page on "
					      + "The Calculationator website:<br><br>"
					      + request.getParameter("message");
			boolean emailSuccessStatus = 
				MailUtil.sendEmail(smtpEmail, smtpEmail, subject, body);
			
			if ( emailSuccessStatus ) {
				message = "<div class='login-registration-success'>"
					+ "Your message has been sent &mdash; thank you!</div>";
			} else {
				message = "<div class='login-registration-error'>"
						+ "Something went wrong. Try your message again!</div>";
			} // end if
			request.setAttribute("message", message);
			
        } // end OUTER if/else
		
        /* Note: this is a bit of a hack to avoid an IllegalStateException */
        if ( redirectStatus ) {
        	response.sendRedirect(request.getContextPath() + url );
        } else {
        	getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
        } // end if 
			
	} // end method doPost

} // end class ContactServlet