package edu.uscb.csci470sp22;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/* Note that this class uses the more modern approach to mapping
 * servlets to URL patterns; we used the older/legacy approach for
 * CalculatorServlet.java (see src/main/webapp/WEB-INF/web.xml), but
 * I wanted to show you this "annotation-style" approach as well, just
 * to ensure you have knowledge of both. :-)
 */
@WebServlet("/process-registration")
public class UserRegistrationServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1945005152368249025L;

	/*
	 * Including doGet here ensures that if someone tries browsing to
	 * the /process-registration link (without submitting a form), then 
	 * they will be automatically redirected to the register.jsp page
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// get current action
        String action = request.getParameter("action");

        /*
         * User will be directed to register.jsp regardless of 
         * whether they registered successfully or if they reached
         * the servlet accidentally
         */
		String url = "/register.jsp";
		String message = "";
		boolean redirectStatus = false;
		
		if (action == null) {
        	url = "/register.jsp"; // user will be directed to register page
        	redirectStatus = true;
        } else if (action.equals("register-new-user")) { 

			String email = request.getParameter("email");
	        
			/* 
			 * if user exists in DB, print message "an account with that
			 *    email address already exists. Did you mean to login?"
			 * if user does not exist, re-load the page with message of
			 *    confirmation email being sent with an auto-generated password
			 */
			try {
				if ( UserDB.userExists(email)  ) {
					message = "<div class='login-registration-error'>"
							  + "Oops! An account with that email address already exists. " 
							  + "Did you mean to <a href='login.jsp'>log in</a> instead?</div>";
				} else {
					
					try {
						// create user & password and add to database
						String password = PasswordUtil.generatePassword();				
						User user = new User(email, password);
						message = "<div class='login-registration-success'>"
								  + "Thank you for signing up! A confirmation message with an "
								  + "automatically generated password has been sent to "
								  + "you at your newly-registered email address.<br><br>"
								  + "You will be redirected to the "
								  + "<a href='login.jsp'>login</a> page in 15 seconds.</div>"
								  + "<meta http-equiv='refresh' content='15;url=login.jsp' />";		
						UserDB.insert(user); // password will be encrypted in database
						
						// send email to new user
						String senderEmail = "calculationator.smh8@yahoo.com";
						String recipientEmail = email;
						String subject = "Welcome to The Calculationator!";
						String body = "Dear " + email + ",<br><br>"
									  + "This message confirms that a new "
									  + "account has been created using your "
									  + "email address. " 
								      + "<br><br>Here is your password:"
								      + "<br><br>" + password
						              + "<br><br>To log in, simply go to "
								      + "<a href='https://calculationator-smh8.herokuapp.com/login.jsp'>" 
								      + "https://calculationator-smh8.herokuapp.com/login.jsp"
								      + "</a><br><br>"
								      + "Thanks and Happy Calculationating!"
								      + "<br><br>"
								      + "- The Calculationator Team";
						MailUtil.sendEmail( senderEmail, recipientEmail, subject, body );
						
					} catch (URISyntaxException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} // end INNER try statement
					
				} // end INNER if/else
			} catch (URISyntaxException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // end OUTER try statement
		
        } // end OUTER if/else
        
        request.setAttribute("message", message);
        
        /* Note: this is a bit of a hack to avoid an IllegalStateException */
        if ( redirectStatus ) {
        	response.sendRedirect(request.getContextPath() + url );
        } else {
        	getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
        } // end if 
        
	} // end method doPost
	
} // end class UserRegistrationServlet