package edu.uscb.csci470sp22;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;

public class DBUtil {

	public static Connection getConnection() throws URISyntaxException, SQLException {
        //URI dbUri = new URI(System.getenv("DATABASE_URL"));
		URI dbUri = new URI("postgres://fcarhwqrozwrld:7f9d4c540fcc8a9cd6154a800eb24d47f6b4c7f45210ff91b69f0b0e0d780031@ec2-34-233-105-94.compute-1.amazonaws.com:5432/db6k5bqaj5tsmk");
        
		String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();

        return DriverManager.getConnection(dbUrl, username, password);
    }
	
    public static void closeStatement(Statement s) {
        try {
            if (s != null) {
                s.close();
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    } // end method closeStatement

    public static void closePreparedStatement(Statement ps) {
        try {
            if (ps != null) {
                ps.close();
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    } // end method closePreparedStatement

    public static void closeResultSet(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    } // end method closeResultSet
    
} // end class DBUtil