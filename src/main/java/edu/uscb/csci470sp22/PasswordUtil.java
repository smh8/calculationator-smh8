package edu.uscb.csci470sp22;

import java.security.SecureRandom;


/**
 * Utility class with methods for generating new passwords
 * 
 * @author smh8@email.uscb.edu
 *
 */
public class PasswordUtil {
	
	/*
	 * Utility method for generating a random 8-character password
	 * 
	 * For reference, the code below is derived from:
	 * https://www.techiedelight.com/generate-random-alphanumeric-password-java/
	 * 
	 * (NOTE: This is VERY simple and is not meant to be the best possible 
	 *  solution, but it should be understandable by anyone who has taken
	 *  two semesters of Java. For other possible approaches, please check out:
	 *  https://www.baeldung.com/java-generate-secure-password
	 */
	public static String generatePassword() {


		// ASCII range - alphanumeric (0-9, a-z, A-Z)
		String passwordCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		
		int passwordLength = 8; // can be adjusted for a longer password if you like
		
		SecureRandom randomNumberGenerator = new SecureRandom();
		StringBuffer passwordBuilder = new StringBuffer();

		/*
		 * Within each iteration of the loop, a character is randomly selected
		 * from the given ASCII range above, and then that character is
		 * appended to our StringBuilder instance 
		 */
		for (int i = 0; i < passwordLength; i++) {
			int randomCharacterIndex = randomNumberGenerator.nextInt(passwordCharacters.length());
			passwordBuilder.append(passwordCharacters.charAt(randomCharacterIndex));
		} // end for

		// Convert StringBuffer object to String and return
		return passwordBuilder.toString();
		
	} // end method generatePassword
	
} // end class PasswordUtil