/**
 * 
 */
package edu.uscb.csci470sp22;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Utility class that provides static methods for working with the User table in 
 * the database. This class is part of the MODEL in the Model-View-Controller 
 * design pattern.
 * 
 * @author brian
 */
public class UserDB {

	/**
	 * 
	 * @param user
	 * @return 1 if insert is successful, 0 otherwise
	 * @throws URISyntaxException 
	 * @throws SQLException 
	 */
    public static int insert(User user) throws URISyntaxException, SQLException {
    	// return 0; /* for mocking/testing */
    	
    	Connection connection = DBUtil.getConnection();
        PreparedStatement ps = null;

        String query = "INSERT INTO users( email, password ) "
            			+ "VALUES(?,MD5(?))";

        try { 
            ps = connection.prepareStatement(query);
            ps.setString(1, user.getEmail());
            ps.setString(2, user.getPassword());
            return ps.executeUpdate(); // will return 1 if successful
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
        	DBUtil.closePreparedStatement(ps);
        	connection.close();
        } // end try statement
        
    } // end method insert

    
    /**
     * Derived from the selectUser method of class UserDB, as explained
     * in Chapter 12 of Murach's Java Servlets and JSP
     * 
     * @param email
     * @param password
     * @return the User object found in the DB, or null if no matching User found
     * @throws SQLException 
     * @throws URISyntaxException 
     */
    public static User checkLogin(String email, String password) 
    		throws URISyntaxException, SQLException {
    	/*
    	// placeholder code for mocking/testing
    	User user = new User(email, password); 
    	return user;
    	*/
    	
    	Connection connection = DBUtil.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        		
        /*
         * Note that the second ? refers to the user's "plain-text"
         * password, but the use of the md5() PostgreSQL function will
         * check to see if the ENCRYPTED version of the password is
         * stored in the database 
         */
        String query = "SELECT * FROM users " 
                       + "WHERE email = ? AND password = md5(?)";

        try { 
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, password);
            rs = ps.executeQuery();
            User user = null;
            if ( rs.next() ) {
            	user = new User();
            	user.setEmail(email);
            	user.setPassword(password);
            }
            return user;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
        	DBUtil.closeResultSet(rs);
        	DBUtil.closePreparedStatement(ps);
        	connection.close();
        } // end try statement
    	
    } // end method checkLogin
    
    /**
     * Derived from the emailExists method of class UserDB, as explained
     * in Chapter 12 of Murach's Java Servlets and JSP
     * 
     * @param email
     * @return true if the user already exists, false otherwise
     * @throws SQLException 
     * @throws URISyntaxException 
     */
    public static boolean userExists(String email) throws URISyntaxException, SQLException {
    	
    	// return false; /* for mocking/testing */
    	
    	Connection connection = DBUtil.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        		
        String query = "SELECT * FROM users WHERE email = ?";

        try { 
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            return rs.next(); // returns true if email exists
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        } finally {
        	DBUtil.closeResultSet(rs);
        	DBUtil.closePreparedStatement(ps);
        	connection.close();
        } // end try statement
        
    } // end method userExists
    
} // end class UserDB