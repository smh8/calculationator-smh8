package edu.uscb.csci470sp22;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/* Note that this class uses the more modern approach to mapping
 * servlets to URL patterns; we used the older/legacy approach for
 * CalculatorServlet.java (see src/main/webapp/WEB-INF/web.xml), but
 * I wanted to show you this "annotation-style" approach as well, just
 * to ensure you have knowledge of both. :-)
 */
@WebServlet("/process-login")
public class UserLoginServlet extends HttpServlet {

	private static final long serialVersionUID = -6911312570093171777L;

	/*
	 * Including doGet here ensures that if someone tries browsing to
	 * the /process-login link (without submitting a form) then they will be 
	 * automatically redirected to the login.jsp page
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	} // end method doGet
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// get current action
        String action = request.getParameter("action");

		String url = "/login.jsp";
		String message = "";
		boolean redirectStatus = false; 
		
        if (action == null) {
        	url = "/login.jsp"; // redirect if user visits /process-login
        	redirectStatus = true;
        } else if (action.equals("authenticate-user")) { 
		
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			
			User user;
			
			try {
				user = UserDB.checkLogin(email, password);
				
				// if successful, redirect to calculate servlet
				if (user != null) {
					HttpSession session = request.getSession(); // this creates session
					session.setAttribute("user", user); // session will have access to both
														// the user's email and password
					
					// get user's calculation history
					String history = CalculationDB.getCalculationHistoryForUser(user.getEmail());
			    	session.setAttribute("history", history);
					
			    	url = "/calculate";
			    	redirectStatus = true;
				} else {
					message = "<div class='login-registration-error'>"
							+ "Hmm... we can't seem to find an account with "
							+ "that email/password combination. "
							+ "Please try signing in again, or you may "
							+ "<a href='register.jsp'>register</a> for "
							+ "a new account.</div>";
					request.setAttribute("message", message);
				} // end INNER if/else
				
			} catch (URISyntaxException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // end 
		
        } // end OUTER if/else
		
        /* Note: this is a bit of a hack to avoid an IllegalStateException */
        if ( redirectStatus ) {
        	response.sendRedirect(request.getContextPath() + url );
        } else {
        	getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
        } // end if 
        
	} // end method doPost

} // end class UserLoginServlet