package edu.uscb.csci470sp22;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Utility class that defines methods for sending emails
 * 
 * @author smh8@email.uscb.edu
 *
 */
public class MailUtil {

	/*
	 * Utility method for sending confirmation email
	 * Loosely based on the sendMail method in the MailUtilLocal class
	 * defined in Chapter 14 of Murach's Java Servlets & JSP 3rd edition
	 * 
	 * NOTE: For this to work, you'll need to add the javax.mail dependency
	 *       to your pom.xml file. In addition, you'll need to set up 
	 *       a Yahoo email account that you can use strictly for sending
	 *       messages using this method
	 */
	public static boolean sendEmail( String senderEmail,
			String recipientEmail, String messageSubject, String messageBody) {
		try {
			// 1 - get a mail session
			Properties props = new Properties();
	        props.put("mail.transport.protocol", "smtps");
	        props.put("mail.smtps.host", "smtp.mail.yahoo.com");
	        props.put("mail.smtps.port", 465);
	        props.put("mail.smtps.auth", "true");
	        Session session = Session.getDefaultInstance(props);
	        session.setDebug(true);
	        
			// 2 - create a message (assume formatted using HTML)
			Message message = new MimeMessage(session);
			message.setSubject( messageSubject );
			message.setContent( messageBody, "text/html" );
			
			// 3 - address the message
			Address fromAddress = new InternetAddress( senderEmail );
			Address toAddress = new InternetAddress( recipientEmail );
			message.setFrom(fromAddress);
			message.setRecipient(Message.RecipientType.TO, toAddress);
			
			// 4 - send the message
			Transport transport = session.getTransport();
			/* TODO: replace these with your own credentials from Yahoo */
	        transport.connect("calculationator.smh8", "odevnralwkjiulbn");
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	        return true;
		} catch (MessagingException e) {
			System.out.println(e);
			return false;
		} // end try
		
	} // end method sendEmail
	
} // end class MailUtil