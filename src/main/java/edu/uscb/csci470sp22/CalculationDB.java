/**
 * 
 */
package edu.uscb.csci470sp22;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author brian
 *
 */
public class CalculationDB {

	/**
	 * 
	 * @param email
	 * @param calculation
	 * @return 1 if query ran successfully
	 * @throws URISyntaxException
	 * @throws SQLException
	 */
	public static int insert( String email, Calculation calculation ) 
			throws URISyntaxException, SQLException  {
		Connection connection = DBUtil.getConnection();
        PreparedStatement ps = null;

        String query = "INSERT INTO calculations(  user_id, first_number, second_number, operation, result ) "
            			+ "VALUES((SELECT user_id FROM users WHERE email = ? LIMIT 1), ?, ?, ?, ?)";

        try { 
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            ps.setDouble(2, calculation.getNum1() );
            ps.setDouble(3, calculation.getNum2() );
            ps.setString(4, calculation.getOperation() );
            ps.setString(5, calculation.getResult() );
            return ps.executeUpdate(); // will return 1 if successful
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
        	DBUtil.closePreparedStatement(ps);
        	connection.close();
        } // end try statement
        
	} // end method insert
	
	/**
	 * 
	 * @param email
	 * @return the calculation history for the user in reverse chronological order and formatted as HTML
	 * @throws URISyntaxException
	 * @throws SQLException
	 */
	public static String getCalculationHistoryForUser( String email ) 
			throws URISyntaxException, SQLException {
				
		Connection connection = DBUtil.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT first_number, second_number, operation, result " 
		        	    + "FROM users INNER JOIN calculations "
		        	    + "ON users.user_id = calculations.user_id " 
		        	    + "WHERE users.email = ?";

        ArrayList<Calculation> calcHistoryList = new ArrayList<>();
        
        try { 
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery(); 
            Calculation calculation = null;
            
            // use a while loop to work with a multi-line result set
            while ( rs.next() ) {
            	double num1 = Double.parseDouble(rs.getString("first_number"));
            	double num2 = Double.parseDouble(rs.getString("second_number"));
            	String operation = rs.getString("operation");
            	calculation = 
            		new Calculation.CalculationBuilder(num1, num2, operation)
            		.result()
            		.build();
            	calcHistoryList.add(calculation);
            } // end while
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
        	DBUtil.closePreparedStatement(ps);
        	connection.close();
        } // end try statement
			
        /* convert ArrayList to HTML string */
		StringBuffer historyBuilder = new StringBuffer();
		historyBuilder.append("<div class='history'>");
		historyBuilder.append("<p>Your Calculation History</p>");
		for (int record = calcHistoryList.size()-1; record >= 0; record-- ) {
			historyBuilder.append( calcHistoryList.get(record).getNum1() );
			historyBuilder.append(" ");
			historyBuilder.append( calcHistoryList.get(record).getOperatorSymbolHtml() );		
			historyBuilder.append(" ");
			historyBuilder.append( calcHistoryList.get(record).getNum2() );
			historyBuilder.append(" = ");
			historyBuilder.append( calcHistoryList.get(record).getResult() );
			historyBuilder.append("<br>");
		} // end for
		historyBuilder.append("</div>");
		
 		return historyBuilder.toString();
        
	} // end method getCalculationHistoryForUser
	
	
} // end class CalculationDB