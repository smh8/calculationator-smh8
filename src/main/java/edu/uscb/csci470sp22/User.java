package edu.uscb.csci470sp22;

import java.io.Serializable;

/*
 * NOTE: Serializable is a "marker interface" that is used
 * to tell Java that objects instantiated from this class
 * can be serialized (written to a file), de-serialized
 * (read from a file), or otherwise transmitted across a network.
 * Since it is a "marker" interface, you don't need to
 * implement any methods of the interface.
 */
public class User implements Serializable {

	private static final long serialVersionUID = 6775923872710550798L;
	private String email;
	private String password;
	
	public User() {
		email = "";
		password = "";
	} // end no-arg constructor

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	} // end 2-arg constructor

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
} // end class User
