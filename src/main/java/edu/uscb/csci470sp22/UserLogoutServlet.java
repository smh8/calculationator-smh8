package edu.uscb.csci470sp22;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/* Note that this class uses the more modern approach to mapping
 * servlets to URL patterns; we used the older/legacy approach for
 * CalculatorServlet.java (see src/main/webapp/WEB-INF/web.xml), but
 * I wanted to show you this "annotation-style" approach as well, just
 * to ensure you have knowledge of both. :-)
 */
@WebServlet("/process-logout")
public class UserLogoutServlet extends HttpServlet {

	private static final long serialVersionUID = 3920363199235536663L;

	/*
	 * Why only doGet here? Because there's presently no way for a POST
	 * request to make its way to this servlet (remember, POST requests
	 * only come from FORMS!)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// Passing in the `false` argument means that we will call an 
		// overloaded version of the getSession method that will NOT create
		// a new session if there is no existing session
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute("user");
			session.removeAttribute("history");
			
			//RequestDispatcher dispatcher = request.getRequestDispatcher("calculate");
			//dispatcher.forward(request, response);
			
			response.sendRedirect(request.getContextPath() + "/calculate");
		} // end if
		
	} // end method doGet

} // end class UserLogoutServlet